#!/bin/bash

# Installation Firefox Development

echo "Updating your system"
sleep 1
sudo apt update
echo
echo "Removing Firefox old"
sleep 1
sudo apt remove -y firefox
sudo apt --fix-broken install
echo
echo "Installing Firefox Development"
sleep 1
cd Downloads
sudo su
tar -xvf firefox-*.tar.bz2
echo
echo "Moving firefox to /opt"
sleep
mv firefox /opt
echo
echo "Creating a symlink"
sleep 1
ln -s /opt/firefox/firefox /usr/local/bin/firefox
echo
echo "Creating a Desktop shortcut"
sleep 1
touch /usr/share/applications/firefox-developer.desktop
echo
echo "Copy this code into firefox-developer.desktop"
sleep 1
echo "
[Desktop Entry]
Name=Firefox Developer
GenericName=Firefox Developer Edition
Exec=/usr/local/bin/firefox
Terminal=false
Icon=/opt/firefox/browser/chrome/icons/default/default48.png
Type=Application
Categories=Application;Network;X-Developer;
Comment=Firefox Developer Edition Web Browser
"
read -p "If would you already copied this code, confirm this step and paste it in the next stage [y/n]: " s1
if [[ -z $s1 ]]
then
	while [ -z $request ]
	do
		read -p "You forgot to answer to the previous condition! " request
		echo $request
	done
else
	echo "Copy the code here!"
	sleep 3
	vim /usr/share/applications/firefox-developer.desktop
fi
echo "Adding permission on firefox-developer.desktop"
sleep 1
chmod +x /usr/share/applications/firefox-developer.desktop
echo
echo "Installation finish"
