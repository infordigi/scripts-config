#!/bin/bash

# Instalação do FileZilla
read -p "Do you want update your system: [y/n] " update
if [ "$update" == "y" ]; then
  echo "Update system..."
  sleep 1
  sudo apt update
  echo
fi
echo "Add repository's program..."
sleep 1
sudo sh -c 'echo "deb http://archive.getdeb.net/ubuntu $(lsb_release -cs)-getdeb apps" >> /etc/apt/sources.list.d/getdeb.list'
echo "Add repository's key..."
sleep 1
wget -q -O - http://archive.getdeb.net/getdeb-archive.key | sudo apt-key add -
echo "Update system.."
sleep 1
sudo apt update
echo "Installing FileZilla..."
sudo apt install -y filezilla
echo