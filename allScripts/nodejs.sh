#!/bin/bash

# Instalação do nodejs
echo "NodeJS installation"
read -p "Do you want update your system? [y/n]: " update
if [ "$update" == "y" ]; then
  echo "Updatting system..."
  sleep 3
  sudo apt update
  echo
fi

echo "Installing PPA repository"
sleep 3
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
curl -sL https://deb.nodesource.com/setup_8.x | sudo bash -
echo

echo "Installing NodeJS"
sudo apt install -y nodejs
echo

echo "Your Nodejs version"
node --version
sleep 2

