#!/bin/bash

#INSTALAÇÃO DO nvm
echo "NVM and NodeJS Installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Installing nvm..."
sleep 3
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.35.2/install.sh | bash
echo

echo "Setting the environment variable..."
sleep 2
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

echo "setting source ~/.bashrc OR source ~/.zshrc"
read -p "Setting your command" setsource
$setsource
echo

echo "Your nvm version:"
sleep 2
nvm --version
echo

#INSTALAÇÃO DO nodeJS
echo "Installing NodeJS..."
echo "set up this command below: 'nvm install 10.15.1'"
read -p "set here: " setinstall
$setinstall
echo

echo "set up this command below: 'nvm use 10.15.1'"
read -p "set here: " setuse
$setuse
echo

echo "Your node version:"
node --version
sleep 2
echo