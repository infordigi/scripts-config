#!/bin/bash

# Instalação do Vagrant
read -p "Do you want update your system? [y/n]: " update
if [ "$update" == "y" ]; then
  echo "Update system..."
  sleep 2
  sudo apt update
  echo
fi
echo "Download Vagrant package..."
sleep 1
curl -O https://releases.hashicorp.com/vagrant/2.2.6/vagrant_2.2.6_x86_64.deb
echo "Installing Vagrant..."
sleep 1
sudo apt install ./vagrant_2.2.6_x86_64.deb
echo "Your Vagrant version"
sleep 3
vagrant --version
echo