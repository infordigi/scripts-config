sudo chmod +x /usr/local/bin/docker-compose#!/bin/bash

#INSTALAÇÃO DO docker-compose
echo "Docker Compose installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Updating yuor system..."
sleep 2
sudo apt update
echo

echo "Download the Docker Compose binary..."
sleep 2
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
echo

echo "Applying and executing permissions to the Compose binary..."
sleep 2
sudo chmod +x /usr/local/bin/docker-compose
sudo chown $USER /usr/local/bin/docker-compose
echo

echo "Your Compose version:"
sudo docker-compose --version
sleep 3
echo
