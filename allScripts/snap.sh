#!/bin/bash

#Instalação PyCharm, IteliJ e Slack
read -p "Do you want update your system? [y/n]: " update
if [ "$update" == "y" ]; then
  echo "Update system..."
  sleep 2
  sudo apt update
  echo
fi
read -p "Do you want install PyCharm? [y/n]: " pycharm
if [ "$pycharm" == "y" ]; then
  echo "Installing PyCharm..."
  sleep 2
  sudo snap install pycharm-community --classic
  echo
fi
read -p "Do you want install IteliJ? [y/n]: " intelij
if [ "$intelij" == "y" ]; then
  echo "Installing IteliJ..."
  sleep 2
  sudo snap install intellij-idea-community --classic
  echo
fi
read -p "Do you want install Slack? [y/n]: " slack
if [ "$slack" == "y" ]; then
  echo "Installing Slack..."
  sleep 2
  sudo snap install slack --classic
  echo
fi
read -p "Do you want install MySQL-WorkBench? [y/n]: " workbench
if [ "$workbench" == "y" ]; then
  echo "Installing Workbench..."
  sleep 2
  sudo apt install -y mysql-workbench
  echo
fi
read -p "Do you want install VLC? [y/n]: " vlc
if [ "$vlc" == "y" ]; then
  echo "Installing VLC..."
  sleep 2
  sudo apt remove vlc --auto-remove
  sudo add-apt-repository ppa:videolan/stable-daily
  sudo apt update
  sudo apt install -y vlc
  echo
fi
