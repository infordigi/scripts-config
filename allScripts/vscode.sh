#!/bin/bash

#INSTALAÇÃO DO VScode
echo "VScode installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Importing the Microsoft GPG key..."
sleep 2
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
echo

echo "Enable the Visual Studio Code repository..."
sleep 2
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
echo

echo "Installing the latest versioin VScode..."
sleep 2
sudo apt install -y code
echo