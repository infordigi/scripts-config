#!/bin/bash

#INSTALAÇÃO DO VirtualBox
echo "VirtualBox 6 installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Importing the GPG keys..."
sleep 2
wget https://download.virtualbox.org/virtualbox/6.1.2/VirtualBox-6.1.2-135662-Linux_amd64.run -O virtualbox.run
echo

echo "Make a file executable"
sleep 2
chmod +x virtualbox.run
echo

echo "Start installation"
sleep 2
sudo ./virtualbox.run
echo