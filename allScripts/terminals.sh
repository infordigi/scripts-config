#!/bin/bash

#Instalação do terminal produtivo
echo "Produtive Terminals"
read -p "Do you want to update the system? (yes/no): " update
if [ "$update" == "y" ]; then
	echo "Updating system..."
	sleep 3
	sudo apt update
	echo
fi
echo "Install any applications for terminals"
echo "Installing ZSH..."
sleep 2
curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh; zsh
echo

echo "Downloading Hack fonts..."
sleep 2
wget -c https://github-production-release-asset-2e65be.s3.amazonaws.com/27574418/84e420dc-35cf-11e8-8df7-3eed56d60859
echo

echo "Uncompress Hack font to ~/.fonts"
sleep 3
sudo mkdir ~/.fonts \
	&& cd ~/.fonts \
	&& sudo unzip ~/Downloads/Hack.zip
