#!/bin/bash

#INSTALAÇÃO DO Docker
echo "Docker installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi

echo "Uninstall old version"
sleep 2
sudo apt-get remove docker docker-engine docker.io containerd runc
echo

echo "Install the dependencie necessary..."
sleep 2
sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
echo

echo "Import the repository’s GPG key..."
sleep 2
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
echo

echo "Add the Docker APT repository..."
sleep 2
sudo bash -c 'echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu disco stable" > /etc/apt/sources.list.d/docker-ce.list'
echo

echo "Update package and install the latest version of Docker CE..."
sleep 3
sudo apt update 
sudo apt install -y docker-ce
echo

echo "Your Docker version:"
sudo docker -v
sleep 3
echo

echo "Adding $USER in Docker-Group..."
sleep 2
sudo usermod -aG docker ${USER}
su ${USER}
id -nG
sleep 2
#sudo chown $USER:docker /var/run/docker.sock
#sudo chown $USER:$USER /usr/bin/docker
echo

echo "Pull hello-world container..."
sleep 2
sudo docker container run hello-world
echo

echo "Docker successfully installed"
sleep 3
echo
