#!/bin/bash

echo "adding yarn repository and apt-key"
sleep 2
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
echo

echo "Updatting and installing Yarn 1.21.1"
sleep 2
sudo apt update
sudo apt install --no-install-recommends yarn
echo

# echo "Adding path in your profile[.profile, .bash_porfile, .bashrc or .zshrc]"
# echo 'Set this paht: export PATH="$PATH:`yarn global bin`"'
# read -p "set here: " setpath
# $setpath
# echo