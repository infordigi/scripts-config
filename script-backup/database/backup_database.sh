#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import commands
import subprocess

# erase old backup
cleaner = os.system("sudo sh -c 'rm -rf dump_*.sql'")

# backup new file from database postgresql
backupdb = os.system("sudo sh -c 'docker exec -t postgresSQl pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y\"_\"%H_%M_%S`.sql'")

# copy new backup to bucket s3
send_copy = os.system("sudo sh -c 'aws s3 cp /home/ubuntu/dump_*.sql s3://urbans-backups/airbitchange-backup/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers'")

# erase new backup
cleaner = os.system("sudo sh -c 'rm -rf dump_*.sql'")