#!/bin/bash

echo "Download Vagrant"
wget https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_x86_64.deb

echo "Install Vagrant"
sudo dpkg -i vagrant_2.2.5_x86_64.deb

echo "Vagrant version"
vagrant --version